/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author ASUS
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import com.mycompany.gui.MasyarakatSekitar;

public class dialog_tmbhMasyarakat extends JDialog implements ActionListener {
    private JLabel label_title;
    private JButton button_tmbh;
    private JLabel label_nama;
    private JLabel label_no;
    private JLabel label_tglLahir;
    private JTextField textField_nama;
    private JTextField textField_no;
    private JTextField textField_tglLahir;

    public dialog_tmbhMasyarakat (){
        init();
    }
    
    public void init(){
        this.setLayout(null); 
        label_title = new JLabel("Tambah Masyarakat Sekitar");
        label_title.setBounds(110, 10, 200, 20);
        this.add(label_title);
        
        label_nama = new JLabel("Nama Masyarakat:");
        label_nama.setBounds(122, 60, 100, 50);
        this.add(label_nama);
        textField_nama = new JTextField();
        textField_nama.setBounds(180, 75, 180, 20);
        this.add(textField_nama);
        
         label_no = new JLabel("Nomor:");
        label_no.setBounds(117, 100, 100, 50);
        this.add(label_no);
        textField_no = new JTextField();
        textField_no.setBounds(180, 115, 180, 20);
        this.add(textField_no);
        
        label_tglLahir = new JLabel("Tempat, Tanggal Lahir:");
        label_tglLahir.setBounds(20, 155, 200, 20);
        this.add(label_tglLahir);
        textField_tglLahir = new JTextField();
        textField_tglLahir.setBounds(180, 155, 180, 20);
        this.add(textField_tglLahir);
        
        button_tmbh = new JButton("Tambah");
        button_tmbh.setBounds(150, 200, 100, 30);
        this.add(button_tmbh);


}
    
@Override
    public void actionPerformed(ActionEvent e) {
if (e.getSource() == button_tmbh) {

                MasyarakatSekitar masya = new MasyarakatSekitar();
                masya.setNama(textField_nama.getText());
                masya.setNomor(textField_no.getText());
                masya.setTemptLahir(textField_tglLahir.getText());
                main.anggota[main.jumlah] = masya;
                main.jumlah++;
                JOptionPane.showMessageDialog(null, textField_nama.getText() + "\nData masyarakat tersimpan");
                this.dispose();
        }

    }

   

    
    
}

