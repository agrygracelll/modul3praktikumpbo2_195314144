/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JOptionPane;
import com.mycompany.gui.UKM;
import com.mycompany.gui.Penduduk;

public class dialog_tmbhUkm extends JDialog implements ActionListener {
    private JLabel label_title;
    private JButton button_tmbh;
    private JLabel label_namaUnit;
    private JLabel label_ketua;
    private JLabel label_sekre;
    private JLabel label_penduduk;
    private JComboBox comBox_ketua;
    private JComboBox comBox_sekre;
    private JTextField textField_namaUnit;
    private JTextField textField_ketua;
    private JTextField textField_sekre;
    private JComboBox comBox_penduduk;
    


    private String pendudukArray[] = {"Agry", "Viony", "Dita", "Dina"};

    public dialog_tmbhUkm() {
        init();
    }

    public void init() {
        this.setLayout(null);
        //.setBounds untuk mengatur posisi x, y dan widht, height suatu component
        
        label_title = new JLabel("Tambah UKM");
        label_title.setBounds(110, 10, 200, 20);
        this.add(label_title);
        
        label_namaUnit = new JLabel("Nama unit:");
        label_namaUnit.setBounds(80, 60, 100, 50);
        this.add(label_namaUnit);
        textField_namaUnit = new JTextField();
        textField_namaUnit.setBounds(180, 75, 180, 20);
        this.add(textField_namaUnit);
        
        label_ketua = new JLabel("Ketua UKM:");
        label_ketua.setBounds(80, 100, 100, 50);
        this.add(label_ketua);
        textField_ketua = new JTextField();
        textField_ketua.setBounds(180, 115, 180, 20);
        this.add(textField_ketua);
        
        label_sekre = new JLabel("Sekretaris UKM:");
        label_sekre.setBounds(80, 140, 100, 50);
        this.add(label_sekre);
        textField_sekre = new JTextField();
        textField_sekre.setBounds(180, 155, 180, 20);
        this.add(textField_sekre);
        
        label_penduduk = new JLabel("Anggota\t :");
        label_penduduk.setBounds(80, 180, 100, 50);
        this.add(label_penduduk);
        comBox_penduduk = new JComboBox(pendudukArray);
        comBox_penduduk.setBounds(180, 195, 180, 20);
        this.add(comBox_penduduk);
        
        
        button_tmbh = new JButton("Tambah");
        button_tmbh.setBounds(150, 250, 100, 30);
        this.add(button_tmbh);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
         if (e.getSource() == button_tmbh) {

               UKM ukm = new UKM();
                ukm.setNamaUnit(textField_namaUnit.getText());
//                ukm.setKetua(textField_ketua);
//                ukm.setSekre(textField_sekre);
                JOptionPane.showMessageDialog(null, textField_namaUnit.getText() + "\nData UKM tersimpan");
                this.dispose();
            

        }
    }

}
