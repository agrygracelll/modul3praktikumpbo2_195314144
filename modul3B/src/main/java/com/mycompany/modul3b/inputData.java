
package com.mycompany.modul3b;
import javax.swing.*;
import java.awt.Color;
import java.awt.Container;
/**
 *
 * @author ASUS
 */
public class inputData extends JFrame {
   private static final int FRAME_WIDTH =300;
    private static final int FRAME_HEIGHT =200;
    private static final int FRAME_X_ORIGIN =300;
    private static final int FRAME_Y_ORIGIN =100;
    private static final int BUTTON_WIDTH =80;
    private static final int BUTTON_HEIGHT =40;
    private final JLabel label_nama;
    private final JLabel label_jenisKel;
    private final JLabel label_hobi;
    private final JTextField text_nama;
    private final JRadioButton radioLaki;
    private final JRadioButton radioPer;
    private final JCheckBox boxOlahraga;
    private final JCheckBox boxShopping;
    private final JCheckBox boxCompGames;
    private final JCheckBox boxBioskop;
    private final JButton cancelButton;
    private final JButton okButton;
    private JButton txtField;
    
    public static void main(String[] args) {
        inputData frame = new inputData();
        frame.setVisible(true);
        
    }
   public inputData(){
        Container contentPane= getContentPane();
        
        setSize(500, 500);
        setResizable(true);
        setTitle("Input Data");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        
        contentPane.setLayout(null);
        contentPane.setBackground(Color.white);
        
        label_nama =new JLabel("Nama \t :");
        label_nama.setBounds(20, 14, 100, 50);
        this.add(label_nama);
        text_nama = new JTextField();
        text_nama.setBounds(120, 30, 180, 20);
        this.add(text_nama);
        
        label_jenisKel = new JLabel("Jenis Kelamin \t: ");
        label_jenisKel.setBounds(20, 40, 100, 50);
        this.add(label_jenisKel);
        
        radioLaki = new JRadioButton("Laki-laki");
        radioLaki.setBounds(120, 55, 80, 20);
        this.add(radioLaki);
        
        radioPer = new JRadioButton("Perempuan");
        radioPer.setBounds(210, 55, 90, 20);
        this.add(radioPer);
        
        label_hobi = new JLabel("Hobi \t: ");
        label_hobi.setBounds(20, 65, 100, 50);
        this.add(label_hobi);
        
        boxOlahraga = new JCheckBox("Olahraga");
        boxOlahraga.setBounds(120, 80, 90, 20);
        this.add(boxOlahraga);
        
        boxShopping = new JCheckBox("Shopping");
        boxShopping.setBounds(120, 105, 90, 20);
        this.add(boxShopping);
        
        boxCompGames= new JCheckBox("Computer Games");
        boxCompGames.setBounds(120, 130, 130, 20);
        this.add(boxCompGames);
        
        boxBioskop = new JCheckBox("Nonton Bioskop");
        boxBioskop.setBounds(120, 155, 130, 20);
        this.add(boxBioskop);
        
        okButton = new JButton("OK");
        okButton.setBounds(100, 200, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(okButton);
        
        cancelButton=new JButton("Cancel");
        cancelButton.setBounds(200, 200, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(cancelButton);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
}
