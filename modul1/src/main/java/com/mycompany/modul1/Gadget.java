
package com.mycompany.modul1;
public class Gadget { 
 protected String merk, warna, prosesor,ram;
  public void setMerk(String merk){
     this.merk= merk;
 }
 public String getMerk(){
     return merk;
 }
 public void setWarna(String warna){
     this.warna= warna;
 }
 public String getWarna(){
     return warna;
 }
 public void setProsesor(String prosesor){
     this.prosesor= prosesor;
 }
 public String getProsesor(){
     return prosesor;
 }
 public void setRam(String ram){
     this.ram= ram;
 }
 public String getRam(){
     return ram;
 }
}
